require('dotenv').config()
import express, { Request, Response } from 'express';
import cors from 'cors';
import bodyParser from "body-parser";
import router from './api/route/logParser';

const app = express();

app.use(cors())

app.use(bodyParser.json())

app.use(process.env.API_PREFIX || '/api', router);

app.get('/', (req: Request, res: Response | any) => {
	res.send("i am here")
});

app.listen(process.env.PORT || 3009,
	() => console.log(`Server has started on port ${process.env.PORT || 3009}`));
