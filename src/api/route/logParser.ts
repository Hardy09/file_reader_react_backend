import express, { Request, Response } from 'express';
import multer from 'multer';
import path from 'path';
import fs from 'fs';
const router = express.Router();

var storage = multer.diskStorage({
  destination: function (req: any, file: any, cb: any) {
    cb(null, 'src/assets')
  },
  filename: function (req: any, file: any, cb: any) {
    cb(null, Date.now() + path.extname(file.originalname))
  }
})

var upload = multer({ storage: storage });

router.post('/log-parser', upload.single("image"),
  async function (req: Request | any, res: Response | any) {
    try {
      const dir = 'src/assets/';

      fs.readFile(dir + req.file.filename, 'utf-8', function (err, jsonData) {
        if (err) {
          console.log("err", err);
          res.json({result: {msg: "error", data: {}}});
          return
        }
        let jsonObject = jsonData.split('\r\n');
        let data: any = []
        for (const item of jsonObject) {

          const transStr = item?.split(' - ')[2]
          const transData = transStr.slice(transStr.indexOf('{'), transStr.lastIndexOf('}') + 1)
          
          let temp: object = {
            timestamp: item?.split(' - ')[0] ? item.split(' - ')[0] : "",
            loglevel: item?.split(' - ')[1] ? item.split(' - ')[1] : "",
            transactionId: transData ? JSON.parse(transData)?.transactionId : "",
            err: transData ? JSON.parse(transData)?.details : "",
            userId: transData ? JSON.parse(transData)?.userId || JSON.parse(transData)?.user?.id : ""
          }
          data.push(temp)
        }

        fs.unlinkSync(dir + req.file.filename);

        res.json({ result: { msg: "success", data: data } });
      });
    } catch (error) {
      console.log("error", error);
      return res.status(403);
    }
  }
);

export default router;